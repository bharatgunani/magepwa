<?php
namespace MageArray\SubcategoriesGridList\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Easycatalog extends Template implements BlockInterface
{

    protected $_template = "widget/easycatalog.phtml";

    public function getCatalogCategory()
    {
        $cat = $this->getCategory();
        $idPath = [];
        $idPath = explode('/', $cat);
        return $idPath[1];
    }
}
