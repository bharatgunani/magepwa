<?php

namespace MageArray\SubcategoriesGridList\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use MageArray\SubcategoriesGridList\Helper\Data as DataHelper;
use Magento\Framework\Registry;

class CategoryThumbnail implements ObserverInterface
{
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    public function __construct(
        DataHelper $helper,
		Registry $coreRegistry        
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_dataHelper = $helper;
    }

    public function execute(EventObserver $observer)
    {
        if ($this->_dataHelper->isEnabled()) {
            $controller = $observer->getFullActionName();
            if ($controller == 'catalog_category_view') {
                $category = $this->_coreRegistry->registry('current_category');
				if($category) {
					$show = $category->getShowSubcategoriesGridList();
					$subCategories = $this->_dataHelper->getSubCategories($category);
					$sub = $category->getChildrenCategories();

					/* if ($show && $subCategories->getSize()) {
						$resultPage = \Magento\Framework\App\ObjectManager::getInstance()
							->get('Magento\Framework\View\Result\Page');
						$pageConfig = $resultPage->getConfig();
						$resultPage->getLayout()->getUpdate();
						$layout = $observer->getEvent()->getLayout();
						//$pageConfig->setPageLayout('1column');
						//$layout->getUpdate()->addUpdate('<referenceBlock name="catalog.leftnav" remove="true"/><referenceContainer name="sidebar.additional" remove="true"/>');
						$layout->getUpdate()->load();
						$layout->generateXml();
					} */
				}
            }
        }
    }
}
