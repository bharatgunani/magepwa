<?php

namespace MageArray\SubcategoriesGridList\Plugin\Catalog\Block\Product;
use MageArray\SubcategoriesGridList\Helper\Data as DataHelper;

class ListProduct
{
    /**
     * @var DataHelper
     */
    protected $_dataHelper;

    public function __construct(
        DataHelper $dataHelper,
		\Magento\Catalog\Model\Layer\Resolver $layerResolver
    ) {
		$this->layerResolver = $layerResolver;
        $this->_dataHelper = $dataHelper;
    }
	
    /*public function afterGetTemplate($subject, $proceed)
    {
		if($this->_dataHelper->isEnabled()) {
			$category = $subject->getLayer()->getCurrentCategory();
			if($category && $category->getShowSubcategoriesGridList()) {
				$subCategories = $this->_dataHelper->getSubCategories($category);
				$layer = $this->layerResolver->get();
				$activeFilters = $layer->getState()->getFilters();
				if($subCategories->getSize() && count($activeFilters)==0) {
					$proceed = 'MageArray_SubcategoriesGridList::category/subcategory.phtml';
				}
			}
		}
		
		return $proceed;
    }*/
}