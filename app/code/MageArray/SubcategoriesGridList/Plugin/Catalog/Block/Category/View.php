<?php
namespace MageArray\SubcategoriesGridList\Plugin\Catalog\Block\Category;
use MageArray\SubcategoriesGridList\Helper\Data as DataHelper;

class View
{
	/**
     * @var DataHelper
     */
    protected $_dataHelper;

    public function __construct(
        DataHelper $dataHelper,
		\Magento\Catalog\Model\Layer\Resolver $layerResolver
    ) {
		$this->layerResolver = $layerResolver;
        $this->_dataHelper = $dataHelper;
    }
    public function beforeToHtml(\Magento\Catalog\Block\Category\View $subject)
    {
    	if($this->_dataHelper->isEnabled()) {
       		$category = $subject->getCurrentCategory();
			if($category && $category->getShowSubcategoriesGridList()) {
				if ($subject->getTemplate() === 'Magento_Catalog::category/description.phtml') {
					$subCategories = $this->_dataHelper->getSubCategories($category);
					$layer = $this->layerResolver->get();
					$activeFilters = $layer->getState()->getFilters();
					if($subCategories->getSize() && count($activeFilters)==0) {
						$subject->setTemplate('MageArray_SubcategoriesGridList::category/subcategory.phtml');
					}
				}
			}
            
        }
    }
}