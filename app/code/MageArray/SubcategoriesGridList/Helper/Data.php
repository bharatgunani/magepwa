<?php

namespace MageArray\SubcategoriesGridList\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Data extends AbstractHelper
{
	 /**
     *
     */
    const COLUMN_NO = 6;
    /**
     *
     */
    const XML_CONFIG_PREFIX = 'subcategoriesgridlist/setting';
    /**
     *
     */
    const XML_PATH_ACTIVE = 'enabled';
    /**
     *
     */
    const XML_SUBCATEGORIES_LAYOUT = 'subcategories_layout';
    /**
     *
     */

    const XML_PLACEHOLDER_IMAGE = 'placeholder_image';

    /**
     *
     */
    const XML_CATEGORY_IMAGE_IFTHUMBNAIL_NOT_AVAILABLE = 'category_image';
	
	protected $_subCategories = [];

    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\Gallery\ReadHandler $_galleryHandler,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Framework\Filesystem $filesystem,
		\Magento\Framework\Filesystem\Driver\File $file
    ) {
        parent::__construct($context);
        $this->_scopeConfig = $context->getScopeConfig();
        $this->registry = $registry;
        $this->_galleryHandler = $_galleryHandler;
        $this->assetRepo = $assetRepo;
        $this->_storeManager = $storeManager;
        $this->categoryFactory = $categoryFactory;
		$this->_mediaDirPath = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
		$this->_file = $file;
    }

    /**
     * @param $path
     * @param bool $joinPrefix
     * @return mixed
     */
    public function getConfig($path, $joinPrefix = true)
    {
        if ($joinPrefix) {
            $path = self::XML_CONFIG_PREFIX . '/' . $path;
        }
        return $this->_scopeConfig
            ->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCurrentCategory()
    {
        return $this->registry->registry('current_category');
    }

    public function getPlaceholderImageConfig()
    {
        return $this->getConfig(self::XML_PLACEHOLDER_IMAGE);
    }

    public function getListLayout()
    {
        return $this->getConfig(self::XML_SUBCATEGORIES_LAYOUT);
    }

    public function getCategoryShow()
    {
        $category = $this->getCurrentCategory();
        if ($category) {
            return $category->getShowSubcategoriesGridList();
        }
    }

    public function getCategoryConfig()
    {
        return $this->getConfig(self::XML_CATEGORY_IMAGE_IFTHUMBNAIL_NOT_AVAILABLE);
    }

    public function getCategory($id)
    {
        return $this->categoryFactory->create()->load($id);
    }

    public function isEnabled()
    {
        return $this->getConfig(self::XML_PATH_ACTIVE);
    }

    public function getCategoryThumbUrl($category)
    {
        $url = false;
        $image = $category->getImageThumb();
        if ($image) {
            if (is_string($image)) {
                $url = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $url= $url. 'catalog/category/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }
        return $url;
    }
   
    public function getImagePath($category)
    {
		$same = $category->getSameImageForThumbnail();
		$imgUrl = false;
        if ($same) {
			if($category->getImage()) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('catalog/category/').$category->getImage();
			}
        } else {
			if($category->getImageThumb()) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('catalog/category/').$category->getImageThumb();
			}
        }
		if (!$imgUrl) {
			if ($this->getCategoryConfig() && $category->getImage()) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('catalog/category/').$category->getImage();
            } elseif($placeHolderImage = $this->getPlaceholderImageConfig()) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('placeholder/').$placeHolderImage;
			} elseif($placeHolder = $this->_scopeConfig->getValue('catalog/placeholder/small_image_placeholder')) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('catalog/product/placeholder/').$placeHolder;
			} else {
				$asset = $this->assetRepo->createAsset('Magento_Catalog::images/product/placeholder/small_image.jpg');
				$imgUrl = $asset->getSourceFile();
			} 
        }else if(!$this->_file->isExists($imgUrl)){
			if($placeHolderImage = $this->getPlaceholderImageConfig()) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('placeholder/').$placeHolderImage;
			} elseif($placeHolder = $this->_scopeConfig->getValue('catalog/placeholder/small_image_placeholder')) {
				$imgUrl = $this->_mediaDirPath->getAbsolutePath('catalog/product/placeholder/').$placeHolder;
			} else {
				$asset = $this->assetRepo->createAsset('Magento_Catalog::images/product/placeholder/small_image.jpg');
				$imgUrl = $asset->getSourceFile();
			} 	
		}
        return $imgUrl;
    }
	
    public function getColumnClass($columnno)
    {
        if (!$columnno) {
            $columnno = self::COLUMN_NO;
        } else {
            if ($columnno > self::COLUMN_NO) {
                $columnno = self::COLUMN_NO;
            }
        }
        if ($columnno == '5') {
            $class = "col-md-5";
        } else {
            $width = (12 / $columnno);
            $class = "col-md-" . $width;
        }
        return $class;
    }
	
	public function getSubCategories($category) {
		if(!isset($this->_subCategories[$category->getId()])) {
			$subCategories = $category->getChildrenCategories();
			$subCategories->addAttributeToSelect('image');
			$subCategories->addAttributeToSelect('same_image_for_thumbnail');
			$subCategories->addAttributeToSelect('image_thumb');
			$subCategories->addAttributeToSelect('description');
			$subCategories->addAttributeToSelect('hide_from_category_catalog');
			$subCategories->addAttributeToFilter([
					array('attribute'=> 'hide_from_category_catalog','null' => true),
					array('attribute'=> 'hide_from_category_catalog','eq' => 0)
				],'','left');
			$this->_subCategories[$category->getId()] = $subCategories;
		}
		
		return $this->_subCategories[$category->getId()];
	}
}
