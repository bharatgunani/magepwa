<?php

namespace MageArray\SubcategoriesGridList\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Filesystem\DirectoryList;

/**
 * Class Resize
 * @package MageArray\MaMarketPlace\Helper
 */
class Resize extends AbstractHelper
{
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Image\AdapterFactory $imageFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\Filesystem\Driver\File $file
    ) {
        parent::__construct($context);
        $this->_filesystem = $filesystem;
        $this->_storeManager = $storeManager;
        $this->_directory = $filesystem
            ->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_imageFactory = $imageFactory;
        $this->_assetRepo = $assetRepo;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_file = $file;
    }
    public function imageResize(
        $src,
        $width = 260,
        $height = 150,
        $dir = 'catalog/category/'
    ) {
        $dirImage = $this->getNewDirectoryImage($src, $width, $height);

        $resizeImage = $this->_filesystem
                ->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath($dir) . $dirImage;

        if (!$this->_file->isExists($resizeImage)) {
            $imageResize = $this->_imageFactory->create();
            $imageResize->open($src);
            $imageResize->backgroundColor([255, 255, 255]);
            $imageResize->constrainOnly(true);
            $imageResize->keepTransparency(true);
            $imageResize->keepFrame(true);
            $imageResize->keepAspectRatio(true);
            $imageResize->resize($width, $height);
            $destination = $resizeImage;
            $imageResize->save($destination);
        }

        return $this->_storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) .
        $dir . $dirImage;
    }

    /**
     * @param $src
     * @param $width
     * @param $height
     * @return string
     */
    public function getNewDirectoryImage($src, $width, $height)
    {
        $segments = array_reverse(explode('/', $src));
        $firstDir = substr($segments[0], 0, 1);
        $secondDir = substr($segments[0], 1, 1);
        return 'cache/' .
        $width .
        'x' .
        $height .
        '/' .
        $firstDir .
        '/' .
        $secondDir .
        '/' .
        $segments[0];
    }
}
