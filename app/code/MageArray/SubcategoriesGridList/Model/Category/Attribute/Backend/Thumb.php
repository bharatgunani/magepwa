<?php
namespace MageArray\SubcategoriesGridList\Model\Category\Attribute\Backend;

class Thumb extends \Magento\Catalog\Model\Category\Attribute\Backend\Image
{
    private $imageUploader;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
    ) {
        $this->_filesystem = $filesystem;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_logger = $logger;
    }

    private function getImageUploader()
    {
        if ($this->imageUploader === null) {
            $this->imageUploader = \Magento\Framework\App\ObjectManager::getInstance()->get(
                'MageArray\SubcategoriesGridList\CategoryUpload'
            );
        }
        return $this->imageUploader;
    }

    public function beforeSave($object)
    {
        $attrCode = $this->getAttribute()->getAttributeCode();

        if (!$object->hasData($attrCode)) {
            $object->setData($attrCode, null);
        } else {
            $values = $object->getData($attrCode);
            if (is_array($values)) {
                if (!empty($values['delete'])) {
                    $object->setData($attrCode, null);
                } else {
                    if (isset($values[0]['name']) && isset($values[0]['tmp_name'])) {
                        $object->setData($attrCode, $values[0]['name']);
                    } 
                }
            }
        }
        return $this;
    }

    public function afterSave($object)
    {
        $image = $object->getData($this->getAttribute()->getName(), null);

        if ($image !== null) {
            try {
                $this->getImageUploader()->moveFileFromTmp($image);
            } catch (\Exception $e) {
                $this->_logger->critical($e);
            }
        }

        return $this;
    }
}
