<?php

namespace MageArray\SubcategoriesGridList\Setup;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    private $categorySetupFactory;

    public function __construct(CategorySetupFactory $categorySetupFactory)
    {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Category::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $categorySetup->removeAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
			'show_subcategories_grid_list');
        $categorySetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
			'show_subcategories_grid_list', 
			[
                'type' => 'int',
                'label' => 'Show Subcategory Grid/List For This Category',
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => false,
                'sort_order' => 100,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
                'default' => 0,
                'note' => "To show Subcategory list with images on category page",
            ]);
        $categorySetup->removeAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
			'hide_from_category_catalog');
        $categorySetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY, 
			'hide_from_category_catalog',
			[
                'type' => 'int',
                'label' => 'Hide This Category From Category Catalog',
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => false,
                'sort_order' => 102,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
                'default' => 0,
            ]);

        $categorySetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'image_thumb',
			[
                'type' => 'varchar',
                'label' => 'Thumbnail Image',
                'input' => 'image',
                'required' => false,
                'sort_order' => 6,
                'backend' => 'MageArray\SubcategoriesGridList\Model\Category\Attribute\Backend\Thumb',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]);

        $categorySetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'same_image_for_thumbnail', 
			[
                'type' => 'int',
                'label' => 'Use Same Image for Catalog Thumbnail',
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => false,
                'sort_order' => 6,
                'default' => 1,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'Content',
            ]);
        $categorySetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'no_of_columns', 
			[
                'type' => 'int',
                'label' => 'Number Of Columns To Show In Catalog',
                'input' => 'text',
                'required' => false,
                'sort_order' => 7,
                'default' => 6,
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'General Information',
            ]);
    }
}
